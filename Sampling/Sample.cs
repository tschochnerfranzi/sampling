﻿namespace Sampling;

public static class Sample
{
    public static SortedDictionary<MeasurementType, List<Measurement>> SampleMeasurements(DateTime startOfSampling,
        List<Measurement> unsampledMeasurements)
    {
        var unsampledResults = SplitMeasurementTypes(unsampledMeasurements);
        var sampledResults = new SortedDictionary<MeasurementType, List<Measurement>>
        {
            {
                MeasurementType.Temperature,
                SampleMeasurements(unsampledResults[MeasurementType.Temperature], startOfSampling)
            },
            {
                MeasurementType.OxygenSaturation,
                SampleMeasurements(unsampledResults[MeasurementType.OxygenSaturation], startOfSampling)
            },
            {
                MeasurementType.HeartRate,
                SampleMeasurements(unsampledResults[MeasurementType.HeartRate], startOfSampling)
            },
            { MeasurementType.Unknown, unsampledResults[MeasurementType.Unknown] }
        };
        return sampledResults;
    }

    private static void SortMeasurementList(List<Measurement> measurements)
    {
        measurements.Sort((x, y) => DateTime.Compare(x.MeasurementTime, y.MeasurementTime));
    }

    private static SortedDictionary<MeasurementType, List<Measurement>> SplitMeasurementTypes(
        List<Measurement> unsampledMeasurements)
    {
        var result =
            new SortedDictionary<MeasurementType, List<Measurement>>();
        var temperatureList = new List<Measurement>();
        var oxygenSaturationList = new List<Measurement>();
        var heartRateList = new List<Measurement>();
        var unknownMeasurementTypeList = new List<Measurement>();
        foreach (var measurement in unsampledMeasurements)
            switch (measurement.Type)
            {
                case MeasurementType.Temperature:
                    temperatureList.Add(measurement);
                    break;
                case MeasurementType.OxygenSaturation:
                    oxygenSaturationList.Add(measurement);
                    break;
                case MeasurementType.HeartRate:
                    heartRateList.Add(measurement);
                    break;
                default:
                    unknownMeasurementTypeList.Add(measurement);
                    break;
            }

        SortMeasurementList(temperatureList);
        SortMeasurementList(heartRateList);
        SortMeasurementList(oxygenSaturationList);
        SortMeasurementList(unknownMeasurementTypeList);

        result.Add(MeasurementType.Temperature, temperatureList);
        result.Add(MeasurementType.HeartRate, heartRateList);
        result.Add(MeasurementType.OxygenSaturation, oxygenSaturationList);
        result.Add(MeasurementType.Unknown, unknownMeasurementTypeList);
        return result;
    }


    private static List<Measurement> SampleMeasurements(List<Measurement> measurements, DateTime startOfSampling)
    {
        var result = new List<Measurement>();
        var endOfInterval = startOfSampling.AddMinutes(5);
        Measurement sample = null;
        for (var i = 0; i < measurements.Count; i++)
        {   
            //skip intervals without measurements
            while ((measurements[i].MeasurementTime - endOfInterval).TotalMinutes > 5)
                endOfInterval = endOfInterval.AddMinutes(5);
            
            
            if (measurements[i].MeasurementTime <= endOfInterval)
            {
                sample = measurements[i];
                if (i + 1 == measurements.Count) result.Add(sample);
                continue;
            }

            if (sample != null) result.Add(sample);
            sample = measurements[i];
            endOfInterval = endOfInterval.AddMinutes(5);
            if (i + 1 == measurements.Count) result.Add(measurements[i]);
        }

        return result;
    }
}