﻿using System.ComponentModel;

namespace Sampling;

public enum MeasurementType
{
    [Description("temperature")] Temperature,
    [Description("oxygen saturation")] OxygenSaturation,
    [Description("heart rate")] HeartRate,

    [Description("unknown measurement type")]
    Unknown
}