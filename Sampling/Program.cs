﻿using Sampling;

Console.WriteLine("Sampling Example");

var unsortedMeasurements = new List<Measurement>
{
    new(new DateTime(2022, 1, 3, 10, 4, 45), MeasurementType.Temperature, 35.79),
    new(new DateTime(2022, 1, 3, 10, 1, 18), MeasurementType.OxygenSaturation, 98.78),
    new(new DateTime(2022, 1, 3, 10, 9, 7), MeasurementType.Temperature, 35.01),
    new(new DateTime(2022, 1, 3, 10, 12, 54), MeasurementType.HeartRate, 35.01),
    new(new DateTime(2022, 1, 3, 10, 3, 34), MeasurementType.OxygenSaturation, 96.49),
    new(new DateTime(2022, 1, 3, 10, 2, 1), MeasurementType.Temperature, 35.82),
    new(new DateTime(2022, 1, 3, 10, 5, 0), MeasurementType.OxygenSaturation, 97.12),
    new(new DateTime(2022, 1, 3, 10, 5, 1), MeasurementType.OxygenSaturation, 95.08),
    new(new DateTime(2022, 1, 3, 10, 4, 22), MeasurementType.Unknown, 70.99)
};

Console.WriteLine("INPUT:");
foreach (var measurement in unsortedMeasurements) Console.WriteLine(measurement);

Console.WriteLine("OUTPUT:");
var output = Sample.SampleMeasurements(new DateTime(2022, 1, 3, 10, 0, 0), unsortedMeasurements);

foreach (var (measurementType, measurementList) in output)
    if (measurementList.Count > 0)
    {
        Console.WriteLine(measurementType);
        foreach (var measurement in measurementList) Console.WriteLine(measurement);
    }