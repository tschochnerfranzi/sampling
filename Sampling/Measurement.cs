﻿namespace Sampling;

public class Measurement
{
    public Measurement(DateTime measurementTime, MeasurementType type, double measurementValue)
    {
        MeasurementTime = measurementTime;
        Type = type;
        MeasurementValue = measurementValue;
    }

    public MeasurementType Type { get; }

    public DateTime MeasurementTime { get; }

    private double MeasurementValue { get; }

    public override string ToString()
    {
        return "time: " + MeasurementTime + ", value: " + MeasurementValue;
    }
}