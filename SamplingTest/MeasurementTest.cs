using Sampling;

namespace SamplingTest;

public class MeasurementTest
{
    [Test]
    public void ConstructorTest()
    {
        var measurementTime = DateTime.Now;

        var measurement = new Measurement(measurementTime, MeasurementType.HeartRate, 7.8);
        Assert.Multiple(() =>
        {
            Assert.That(measurement.MeasurementTime, Is.EqualTo(measurementTime));
            Assert.That(measurement.Type, Is.EqualTo(MeasurementType.HeartRate));
        });
    }

    [Test]
    public void ToStringTest()
    {
        var measurementTime = DateTime.Now;

        var measurement = new Measurement(measurementTime, MeasurementType.Temperature, 31.9);
        var measurementString = measurement.ToString();
        Assert.That(measurementString, Is.EqualTo("time: " + measurementTime + ", value: " + 31.9));
    }
}