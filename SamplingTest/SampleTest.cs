using Sampling;

namespace SamplingTest;

public class SampleTest
{
    [Test]
    public void EmptySampleTest()
    {
        var sampledMeasurements =
            Sample.SampleMeasurements(new DateTime(2022, 1, 3, 10, 0, 0), new List<Measurement>());
        Assert.That(sampledMeasurements, Is.Not.Null);
        Assert.That(sampledMeasurements.Keys.Count, Is.EqualTo(4));
        Assert.That(sampledMeasurements[MeasurementType.Temperature].Count, Is.EqualTo(0));
        Assert.That(sampledMeasurements[MeasurementType.HeartRate].Count, Is.EqualTo(0));
        Assert.That(sampledMeasurements[MeasurementType.OxygenSaturation].Count, Is.EqualTo(0));
        Assert.That(sampledMeasurements[MeasurementType.Unknown].Count, Is.EqualTo(0));
    }


    [Test]
    public void OneMeasurementTypeSampleTest()
    {
        var dateTime = new DateTime(2022, 1, 3, 10, 4, 45);
        var unsortedMeasurements = new List<Measurement>
        {
            new(dateTime, MeasurementType.Unknown, 35.79)
        };
        var sampledMeasurements = Sample.SampleMeasurements(new DateTime(2022, 1, 3, 10, 0, 0), unsortedMeasurements);
        Assert.That(sampledMeasurements, Is.Not.Null);
        Assert.That(sampledMeasurements[MeasurementType.Temperature].Count, Is.EqualTo(0));
        Assert.That(sampledMeasurements[MeasurementType.HeartRate].Count, Is.EqualTo(0));
        Assert.That(sampledMeasurements[MeasurementType.OxygenSaturation].Count, Is.EqualTo(0));
        Assert.That(sampledMeasurements[MeasurementType.Unknown].Count, Is.EqualTo(1));
        Assert.That(sampledMeasurements[MeasurementType.Unknown][0].Type, Is.EqualTo(MeasurementType.Unknown));
        Assert.That(sampledMeasurements[MeasurementType.Unknown][0].MeasurementTime, Is.EqualTo(dateTime));
    }

    [Test]
    public void CorrectSeparationSampleTest()
    {
        var sortedMeasurements = new List<Measurement>
        {
            new(new DateTime(2022, 1, 3, 10, 1, 18), MeasurementType.OxygenSaturation, 98.78),
            new(new DateTime(2022, 1, 3, 10, 2, 1), MeasurementType.Temperature, 35.82),
            new(new DateTime(2022, 1, 3, 10, 4, 22), MeasurementType.Unknown, 70.99),
            new(new DateTime(2022, 1, 3, 10, 5, 1), MeasurementType.OxygenSaturation, 95.08),
            new(new DateTime(2022, 1, 3, 10, 12, 54), MeasurementType.HeartRate, 35.01)
        };
        var sampledMeasurements = Sample.SampleMeasurements(new DateTime(2022, 1, 3, 10, 0, 0), sortedMeasurements);
        Assert.That(sampledMeasurements, Is.Not.Null);
        Assert.That(sampledMeasurements[MeasurementType.Temperature].Count, Is.EqualTo(1));
        Assert.That(sampledMeasurements[MeasurementType.HeartRate].Count, Is.EqualTo(1));
        Assert.That(sampledMeasurements[MeasurementType.OxygenSaturation].Count, Is.EqualTo(2));
        Assert.That(sampledMeasurements[MeasurementType.Unknown].Count, Is.EqualTo(1));
        var oxygenList = new List<Measurement>
        {
            new(new DateTime(2022, 1, 3, 10, 1, 18), MeasurementType.OxygenSaturation, 98.78),
            new(new DateTime(2022, 1, 3, 10, 5, 1), MeasurementType.OxygenSaturation, 95.08)
        };
        Assert.That(sampledMeasurements[MeasurementType.OxygenSaturation][0].Type, Is.EqualTo(oxygenList[0].Type));
        Assert.That(sampledMeasurements[MeasurementType.OxygenSaturation][0].MeasurementTime,
            Is.EqualTo(oxygenList[0].MeasurementTime));
        Assert.That(sampledMeasurements[MeasurementType.OxygenSaturation][1].Type, Is.EqualTo(oxygenList[1].Type));
        Assert.That(sampledMeasurements[MeasurementType.OxygenSaturation][1].MeasurementTime,
            Is.EqualTo(oxygenList[1].MeasurementTime));

        var temperatureList = new List<Measurement>
        {
            new(new DateTime(2022, 1, 3, 10, 2, 1), MeasurementType.Temperature, 35.82)
        };
        Assert.That(sampledMeasurements[MeasurementType.Temperature][0].Type, Is.EqualTo(temperatureList[0].Type));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][0].MeasurementTime,
            Is.EqualTo(temperatureList[0].MeasurementTime));

        var heartRateList = new List<Measurement>
        {
            new(new DateTime(2022, 1, 3, 10, 12, 54), MeasurementType.HeartRate, 35.01)
        };
        Assert.That(sampledMeasurements[MeasurementType.HeartRate][0].Type, Is.EqualTo(heartRateList[0].Type));
        Assert.That(sampledMeasurements[MeasurementType.HeartRate][0].MeasurementTime,
            Is.EqualTo(heartRateList[0].MeasurementTime));

        var unknownList = new List<Measurement>
        {
            new(new DateTime(2022, 1, 3, 10, 4, 22), MeasurementType.Unknown, 70.99)
        };
        Assert.That(sampledMeasurements[MeasurementType.Unknown][0].Type, Is.EqualTo(unknownList[0].Type));
        Assert.That(sampledMeasurements[MeasurementType.Unknown][0].MeasurementTime,
            Is.EqualTo(unknownList[0].MeasurementTime));
    }

    [Test]
    public void CorrectSamplingTest()
    {
        var unsortedMeasurements = new List<Measurement>
        {
            new(new DateTime(2022, 1, 3, 10, 4, 45), MeasurementType.Temperature, 35.79),
            new(new DateTime(2022, 1, 3, 10, 1, 18), MeasurementType.Temperature, 35.78),
            new(new DateTime(2022, 1, 3, 10, 9, 7), MeasurementType.Temperature, 35.01),
            new(new DateTime(2022, 1, 3, 10, 12, 54), MeasurementType.Temperature, 35.01),
            new(new DateTime(2022, 1, 3, 10, 3, 34), MeasurementType.Temperature, 35.49),
            new(new DateTime(2022, 1, 3, 10, 2, 1), MeasurementType.Temperature, 35.82),
            new(new DateTime(2022, 1, 3, 10, 5, 0), MeasurementType.Temperature, 35.12),
            new(new DateTime(2022, 1, 3, 10, 5, 1), MeasurementType.Temperature, 35.08),
            new(new DateTime(2022, 1, 3, 10, 16, 22), MeasurementType.Temperature, 35.99)
        };
        var sampledMeasurements = Sample.SampleMeasurements(new DateTime(2022, 1, 3, 10, 0, 0), unsortedMeasurements);
        Assert.That(sampledMeasurements, Is.Not.Null);
        Assert.That(sampledMeasurements[MeasurementType.Temperature].Count, Is.EqualTo(4));
        Assert.That(sampledMeasurements[MeasurementType.HeartRate].Count, Is.EqualTo(0));
        Assert.That(sampledMeasurements[MeasurementType.OxygenSaturation].Count, Is.EqualTo(0));
        Assert.That(sampledMeasurements[MeasurementType.Unknown].Count, Is.EqualTo(0));


        var temperatureList = new List<Measurement>
        {
            new(new DateTime(2022, 1, 3, 10, 5, 0), MeasurementType.Temperature, 35.12),
            new(new DateTime(2022, 1, 3, 10, 9, 7), MeasurementType.Temperature, 35.01),
            new(new DateTime(2022, 1, 3, 10, 12, 54), MeasurementType.Temperature, 35.01),
            new(new DateTime(2022, 1, 3, 10, 16, 22), MeasurementType.Temperature, 35.99)
        };
        Assert.That(sampledMeasurements[MeasurementType.Temperature][0].Type, Is.EqualTo(temperatureList[0].Type));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][0].MeasurementTime,
            Is.EqualTo(temperatureList[0].MeasurementTime));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][1].Type, Is.EqualTo(temperatureList[1].Type));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][1].MeasurementTime,
            Is.EqualTo(temperatureList[1].MeasurementTime));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][2].Type, Is.EqualTo(temperatureList[2].Type));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][2].MeasurementTime,
            Is.EqualTo(temperatureList[2].MeasurementTime));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][3].Type, Is.EqualTo(temperatureList[3].Type));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][3].MeasurementTime,
            Is.EqualTo(temperatureList[3].MeasurementTime));
    }

    [Test]
    public void LongIntervalSamplingTest()
    {
        var unsortedMeasurements = new List<Measurement>
        {
            new(new DateTime(2022, 1, 3, 10, 15, 45), MeasurementType.Temperature, 35.79),
            new(new DateTime(2022, 1, 3, 10, 1, 18), MeasurementType.Temperature, 35.78),
            new(new DateTime(2022, 1, 3, 12, 38, 0), MeasurementType.Temperature, 35.49),
            new(new DateTime(2022, 1, 3, 10, 19, 7), MeasurementType.Temperature, 35.01),
            new(new DateTime(2022, 1, 3, 10, 32, 54), MeasurementType.Temperature, 35.01),
            new(new DateTime(2022, 1, 3, 10, 59, 34), MeasurementType.Temperature, 35.49)
        };
        var sampledMeasurements = Sample.SampleMeasurements(new DateTime(2022, 1, 3, 10, 0, 0), unsortedMeasurements);
        Assert.That(sampledMeasurements, Is.Not.Null);
        Assert.That(sampledMeasurements[MeasurementType.Temperature].Count, Is.EqualTo(5));
        Assert.That(sampledMeasurements[MeasurementType.HeartRate].Count, Is.EqualTo(0));
        Assert.That(sampledMeasurements[MeasurementType.OxygenSaturation].Count, Is.EqualTo(0));
        Assert.That(sampledMeasurements[MeasurementType.Unknown].Count, Is.EqualTo(0));


        var temperatureList = new List<Measurement>
        {
            new(new DateTime(2022, 1, 3, 10, 1, 18), MeasurementType.Temperature, 35.78),
            new(new DateTime(2022, 1, 3, 10, 19, 7), MeasurementType.Temperature, 35.01),
            new(new DateTime(2022, 1, 3, 10, 32, 54), MeasurementType.Temperature, 35.01),
            new(new DateTime(2022, 1, 3, 10, 59, 34), MeasurementType.Temperature, 35.49),
            new(new DateTime(2022, 1, 3, 12, 38, 0), MeasurementType.Temperature, 35.49)
        };
        Assert.That(sampledMeasurements[MeasurementType.Temperature][0].Type, Is.EqualTo(temperatureList[0].Type));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][0].MeasurementTime,
            Is.EqualTo(temperatureList[0].MeasurementTime));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][1].Type, Is.EqualTo(temperatureList[1].Type));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][1].MeasurementTime,
            Is.EqualTo(temperatureList[1].MeasurementTime));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][2].Type, Is.EqualTo(temperatureList[2].Type));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][2].MeasurementTime,
            Is.EqualTo(temperatureList[2].MeasurementTime));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][3].Type, Is.EqualTo(temperatureList[3].Type));
        Assert.That(sampledMeasurements[MeasurementType.Temperature][3].MeasurementTime,
            Is.EqualTo(temperatureList[3].MeasurementTime));
    }
}